<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gallery</title>
    <meta name="keywords" content="cooking, photos, food, gallery">
    <meta name="description" content="This website will present gallery of inspiring photos of food.">
	<link rel="stylesheet" type="text/css" href="assets/base.css">
</head>

<body>
<h1 id="galleryHeader">Gallery</h1>
<table style="width:100%;">
    <thead>
    <tr>
        <th>
            <img class="rotate" id="borderImage" src="https://students.usask.ca/images/icons/healthier-food-choices.png" alt="kitchen photo"/>
        </th>

        <th colspan="2">
            <strong>Inspiring food photos</strong>
        </th>

        <th>
            <img class="grow" src="https://pbs.twimg.com/profile_images/588139522249990144/jFumDdaH_400x400.jpg"
                 alt="kitchen photo"/>
        </th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>
            <img id="borderImageStretch" class="grow" src="https://i.pinimg.com/736x/88/1a/20/881a20e721913b08cc00d9f7669c5285--ottolenghi-recipes-yotam-ottolenghi.jpg"
                 alt="picture of ottolenghi">
        </td>
        <td colspan="2">
            <img class="borderColorAnimation grow" src="https://hamiltonfarmersmarket.ca/public/uploads/thumbnail/vendor_latinfoods_gallery1.jpg"
                 alt="picture of latin food"></td>
        <td>
            <img class="borderColorAnimation2 grow" src="https://i.pinimg.com/736x/83/e4/0c/83e40ca57f58acd1a09d2ba7956e48c2--best-veggie-burger-fast-food-chains.jpg"
                 alt="picture of vegan burger">
        </td>
    </tr>
    <tr>
        <td class="transitionParent" colspan="4">
                <img class="transition simpleImageBorder" 
                     src="https://i.pinimg.com/736x/83/e4/0c/83e40ca57f58acd1a09d2ba7956e48c2--best-veggie-burger-fast-food-chains.jpg"
                     alt="picture of pasta">
                <img id="borderImageRepeatFadeOut" 
                     class="transition simpleImageBorder" 
                     src="https://i.pinimg.com/736x/88/1a/20/881a20e721913b08cc00d9f7669c5285--ottolenghi-recipes-yotam-ottolenghi.jpg" 
                     alt="picture of burger"> 
        </td>
    </tr>
    </tbody>
</table>
<?php include("footer_log_info.php"); ?>
</body>
</html>
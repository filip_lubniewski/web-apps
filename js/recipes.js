class Product {
	
	constructor(name, unitPrice, quantity) {
		this.name = name;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
	}

	get totalPrice() {
		return this.quantity * this.unitPrice;
	}
	
}

function minProductsPrice(products) {
	var sum = 0;
	for(var i = 0; i < products.length; i++) {
		sum += products[i].totalPrice;
	}
	return Math.floor(sum);
}
var buttonCounter = 1;
var appendButton;
var mouseInEditText = false;
var textContent = null;
var isCtrlDown = false;
var originTextColor;
var originBgColor;
var currentShading = 0;
var cursorDataBox;

window.onload = onWindowLoaded;

function onWindowLoaded() {
	var buttonsContainer = document.getElementById("buttonsContainer");
	appendButton = document.getElementById("appendButton");
	appendButton.addEventListener("click", function(){appendAnotherButton("Button #" + buttonCounter++, buttonsContainer);}, false);
	createLinkBoxes();
	presentCollectionsInfo();
	bindStyleHandlers();
	setupKeyListeners();
	setupMouseListener();
	setupEmailFocusBlur();
	setupFormButtons();
}

function appendAnotherButton(buttonText, parentElement) {
	var modCounter = buttonCounter % 3;
	var btn;
	if(modCounter == 1) {
		btn = createInserter(buttonText + " inserter");
	} else if(modCounter == 2) {
		btn = createReplacer(buttonText + " replacer");
	} else {
		btn = createSuicider(buttonText + " suicider");
	}
	parentElement.appendChild(btn);
}

function createButtonWithText(buttonText) {
	var btn = document.createElement("BUTTON");
	var text = document.createTextNode(buttonText);
	btn.appendChild(text);
	return btn;
}

function createReplacer(buttonText) {
	var btn = createButtonWithText(buttonText);
	btn.addEventListener("click", function(){replaceWithDestroyer(btn);});
	return btn;
}

function createSuicider(buttonText) {
	var btn = createButtonWithText(buttonText);
	btn.addEventListener("click", function(){selfDestroy(btn);});
	return btn;
}

function createInserter(buttonText) {
	var btn = createButtonWithText(buttonText);
	btn.addEventListener("click", function(){
		insertSuiciderBeforeMe(btn);
		this.value = 'testing change of text';
		setTimeout(function() {
			selfDestroy(btn);
		}, 1000);
	});
	return btn;
}

function selfDestroy(element) {
	element.parentNode.removeChild(element);
}

function insertSuiciderBeforeMe(element) {
	element.parentNode.insertBefore(createSuicider("Inserted before"), element);
}

function replaceWithDestroyer(element) {
	element.parentNode.replaceChild(createSuicider("Replacement"), element);
}

function createLinkBoxes() {
	var links = document.links;
	var linksArray = [];
	for(var i = 0; i < links.length; i++) {
		linksArray.push(createLinkElement(links[i]));
	}
	for(var z = 0; z < linksArray.length; z++) {
		createLinkBlockAndAppend(linksArray[z]);
	}
}

function createLinkElement(link) {
	var created = document.createElement('a');
	created.innerHTML = link.innerHTML;
	created.href = link.href;
	return created;
}

function createLinkBlockAndAppend(item) {
	var elem = document.createElement("DIV"); 
	elem.className = "linkBlock";
	elem.append(item);
	var lastInLinks = document.getElementById("lastInLinks");
	document.getElementById("linksOnPage").insertBefore(elem, lastInLinks);
}

function presentCollectionsInfo() {
	var formsInfo = document.getElementById("formsInfo");
	formsInfo.innerHTML += document.forms.length;
	var imagesInfo = document.getElementById("imagesInfo");
	imagesInfo.innerHTML += document.images.length;
	var anchorsInfo = document.getElementById("anchorsInfo");
	anchorsInfo.innerHTML += document.anchors.length;
	var optionsInfo = document.getElementById("optionsInfo");
	var options = document.getElementsByTagName("option");
	optionsInfo.innerHTML += ("<br>This is first option: " + options.item(0).value);
	optionsInfo.innerHTML += ("<br>This is info about our named option: " + options.namedItem("october-option").value);
}

function bindStyleHandlers() {
	var textColorPicker = document.getElementById("text-color-picker");
	var bgColorPicker = document.getElementById("background-color-picker");
	var fontPicker = document.getElementById("font-picker");
	textContent = document.getElementById("textContent");
	textColorPicker.onchange = function(){
									textContent.style.color = textColorPicker.value;
									originTextColor = textColorPicker.value;
								};
	bgColorPicker.onchange = function(){
								textContent.style.backgroundColor = bgColorPicker.value;
								originBgColor = bgColorPicker.value;
							};
	fontPicker.onchange = function(){
							textContent.style.fontFamily = fontPicker.value;
						};
	textContent.addEventListener("mouseover", function() {
		mouseInEditText = true;
	}, false);
	textContent.addEventListener("mouseout", function() {
		mouseInEditText = false;
	}, false);
}

function setupKeyListeners() {
	document.onkeydown = function (event) {
		if(event.ctrlKey) {
			isCtrlDown = true;
		}
		if(event.shiftKey) {
			if(mouseInEditText && isCtrlDown) {
				if(currentShading < 1 && originBgColor !== undefined) {
					currentShading += 0.1;
					textContent.style.backgroundColor = shadeColor(originBgColor, currentShading);
				}
			}
		}
		if(event.keyCode == 40) { // 40 -> DOWN ARROW
			if(mouseInEditText && isCtrlDown) {
				if(currentShading > -1 && originBgColor !== undefined) {
					currentShading -= 0.1;
					textContent.style.backgroundColor = shadeColor(originBgColor, currentShading);
				}
			}
		}
		if(event.keyCode == 81) { // 81 -> Q KEY
			if(mouseInEditText && isCtrlDown) {
				textContent.style.color = originTextColor;
				textContent.style.backgroundColor = originBgColor;
				currentShading = 0;
			}
		}
	};

	document.onkeyup = function (event) {
		if(!event.ctrlKey) {
			isCtrlDown = false;
		}
	};
}

function shadeColor(color, percent) {   
    var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

function setupMouseListener() {
	cursorDataBox = document.getElementById("fixedRightCorner");
	document.onmousedown = function(event) {
		cursorDataBox.innerHTML = "clientX: " + event.clientX + " clientY: " + event.clientY + " x: " + event.screenX + " y: " + event.screenY;
	};
}

function setupEmailFocusBlur() {
	var emailInput = document.getElementById("input-e-mail");
	var emailErrorBox = document.getElementById("error-info-email");
	emailInput.onblur = function() {
		if (!emailInput.value.includes('@')) { // not email
			emailErrorBox.classList.add('invalid');
			emailErrorBox.innerHTML = 'Please enter a correct email.';
		} else {
			emailErrorBox.classList.remove('invalid');
			emailErrorBox.innerHTML = "example: name@domain.com";
		}
	};
	emailInput.onfocus = function() {
		console.log('onfocus called');
		if (emailErrorBox.classList.contains('invalid')) {
			emailErrorBox.classList.remove('invalid');
			emailErrorBox.innerHTML = "example: name@domain.com";
		}
	};
}

function setupFormButtons() {
	var contactForm = document.getElementById("contact-form");
	document.getElementById("clear-button").onclick = function() {
		contactForm.reset();
	};
	document.getElementById("submit-button").onclick = function() {
		contactForm.submit();
	};
}
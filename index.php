<?php
$cookie_color = isset($_COOKIE["color"]) ? $_COOKIE["color"] : '';
if ($cookie_color == '') {
    $color = isset($_GET['color']) ? $_GET['color'] : '';
    if ($color) {
        $cookie_ttl = 10 + time();
        setcookie('color', $color, $cookie_ttl);
    }
}
?>

<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Cooking website</title>
		<meta name="keywords" content="cooking, recipes, food">
		<meta name="description" content="This website will help you to cook a delicious meal.">
		<style type="text/css">
			em {
				color: gray
			}

			h1, h3 {
				text-align: center;
			}

			p {
				font-family: Sana, Helvetica, serif;
				line-height: 1.5
			}

			.special {
				color: lavender;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="assets/base.css">
	</head>

    <?php
    $cookie_color = isset($_COOKIE["color"]) ? $_COOKIE["color"] : '';
    $color = isset($_GET['color']) ? $_GET['color'] : '';
    if ($cookie_color || $color)
        $bgcolor = $cookie_color ? $cookie_color : $color;
    else
        $bgcolor = "white";
    echo "<body style='background-color:$bgcolor'>";
    ?>

	<body>
		<div class="header">
			<h1>Cooking website</h1>
			<h3 class="special">your guide in the kitchen &amp; much more</h3>
		</div>

		<nav id="mainNavigation">
			<ul>
				<li>
					<a href="">Recipes</a>
					<ul>
						<li>
							<a href="">Organic</a>
							<ul>
								<li><a href="">With meet</a></li>
								<li><a href="">Without meet</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li>
					<a href="">Ingredients</a>
					<ul>
						<li>
							<a href="">Best choice</a>
							<ul>
								<li><a href="">Non-gluten</a></li>
								<li><a href="">Sugar free</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li>
					<a href="">Chiefs</a>
					<ul>
						<li>
							<a href="">Awarded</a>
							<ul>
								<li><a href="">Michelin</a></li>
								<li><a href="">Mastercook</a></li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
		<br><br><br>

		<img class="center single" src="images/kitchen-background.jpeg" width="320" height="220" alt="Food photo">

		<article>
			<h2 style="font-size: 20px; color: darkred;">How I found my cooking passion?</h2>
			<p style="font-size: .9em;">
				It all started one winter evening when talking with my grandma. She asked my if I would like to make
				some
				gingerbreads... At first I was confused as I had no experience in baking and cooking at all, but my
				grandma
				convinced me to try.
				<br>
				Everything we did that evening was not only fun, but also result in a really tasty dainties :))
				<br>
				That's all, this is the origin of my cooking passion. I hope you will enjoy my recipees and also share
				yours
				with others. Happy cooking!
			</p>
		</article>

		<p><a href="#contentList">Jump to content list</a></p>

		<section>
			<h3>What would you find on <u>our</u> website?</h3>
			<section>
				<h4>
					My recipes
				</h4>
				<em>Delicious, various &amp; healthy. All of them were tried and mastered across theł years, you'll be
					provided
					with detailed instructions, ingredients and preparation time.</em>
			</section>
			<br>
			<section>
				<h4>
					User recipes
				</h4>
				<em>Everyone who is passionate about cooking or just made something worth sharing is really welcome.
					Just post
					all necessary info and let everybody taste your miracle.</em>
			</section>
			<br>
			<section>
				<h4>
					Galleries
				</h4>
				<em>Colorful, eyecatching &amp; real! As far as its possible every recipe should be backed up with food
					photo.
					That way you can decide not only with your taste but also with your eyes.</em>
			</section>
		</section>

		<h2>List of contents</h2>
		<nav id="contentList">
			<ul>
				<li>
					<a href="recipes.php">Recipes</a>
				</li>

				<li>
					<a href="recipe-form.php">Submit your own recipe</a>
				</li>
				<li>
					<a href="gallery.php">Food gallery</a>
				</li>

				<li>
					<a href="mailto:coo@king.com">Contact us!</a>
				</li>
				<li>
					<a href="contact-form.php">Coopeartion</a>
				</li>
				<li>
					<a href="change-theme.php">Change theme</a>
				</li>
				<li>
					<a href="diagnostics.php">Check cookies</a>
				</li>
			</ul>
		</nav>
		<?php include("footer_log_info.php"); ?>
	</body>
</html>
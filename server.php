<?php
session_start();

$host = "localhost";
$user = "root";
$password = "";
$dbname = "web_test_db";

$login = "";
$username = "";
$email    = "";
$errors = array();

// connect to database
$db = mysqli_connect($host, $user, $password, $dbname);

// REGISTER USER
if (isset($_POST['registerNewUser'])) {
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $login = mysqli_real_escape_string($db, $_POST['login']);
  $password_1 = mysqli_real_escape_string($db, $_POST['password']);
  $password_2 = mysqli_real_escape_string($db, $_POST['password_repeat']);

  // form validation: ensure that the form is correctly filled
  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($login)) { array_push($errors, "Email is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if ($password_1 != $password_2) {
	array_push($errors, "The two passwords do not match");
  }

  // register user if there are no errors in the form
  if (count($errors) == 0) {
  	$password = md5($password_1);//encrypt the password before saving in the database
  	$query = "INSERT INTO users (username, login, password) 
  			  VALUES('$username', '$login', '$password')";
  	mysqli_query($db, $query);
  	$_SESSION['username'] = $username;
  	header('location: index.php');
  }
}

if (isset($_POST['buttonLogin'])) {
  $login = mysqli_real_escape_string($db, $_POST['login']);
  $password = mysqli_real_escape_string($db, $_POST['password']);

  if (empty($login)) {
  	array_push($errors, "Login is required");
  }
  if (empty($password)) {
  	array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
  	$password = md5($password);
  	$query = "SELECT * FROM users WHERE login='$login' AND password='$password'";
  	$results = mysqli_query($db, $query);
  	if (mysqli_num_rows($results) == 1) {
  	  $row = $results->fetch_assoc();
  	  $_SESSION['username'] = $row['username'];
  	  $_SESSION['success'] = "You are now logged in";
  	  header('location: index.php');
  	}else {
  		array_push($errors, "Wrong username/password combination");
  	}
  }
}


if (isset($_POST['editUserData'])) {
  $login = mysqli_real_escape_string($db, $_POST['login']);
  $password = mysqli_real_escape_string($db, $_POST['password']);
  $new_password = mysqli_real_escape_string($db, $_POST['password_new']);
  if (empty($login)) {
  	array_push($errors, "Login is required.");
  }
  if (empty($password)) {
  	array_push($errors, "Password is required.");
  }
  if (empty($new_password)) {
  	array_push($errors, "New password cannot be empty.");
  }

  if (count($errors) == 0) {
  	$password = md5($password);
  	$new_password = md5($new_password);
  	$query = "SELECT * FROM users WHERE login='$login' AND password='$password'";
  	$results = mysqli_query($db, $query);
  	if (mysqli_num_rows($results) == 1) {
  	  $row = $results->fetch_assoc();
  	  $id = $row['id'];
  	  $query_edit = "UPDATE users SET password='$new_password' WHERE id=$id";
  	  if(mysqli_query($db, $query_edit)) {
    	header('location: index.php');
	  } else {
     	echo "Error updating record: " . mysqli_error($conn);
	  }
  	} else {
  		array_push($errors, "Wrong username/password combination");
  	}
  }
}
 ?>

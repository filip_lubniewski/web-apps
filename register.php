<?php 
include('server.php');
?>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Register</title>
		<link rel="stylesheet" type="text/css" href="assets/base.css">
	</head>
<body style="margin-top:20px;">
<form method="post">
	<?php include('errors.php'); ?>
	<table cellpadding="2" cellspacing="2">
		<tr>
			<td>Username</td>
			<td><input type="text" name="username"></td>
		</tr>
		<tr>
			<td>Login</td>
			<td><input type="text" name="login"></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><input type="password" name="password"></td>
		</tr>
		<tr>
			<td>Repeat password</td>
			<td><input type="password" name="password_repeat"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" name="registerNewUser" value="Register"></td>
		</tr>
	</table>
</form>
</body>
</html>
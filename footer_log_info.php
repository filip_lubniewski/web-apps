<?php 
session_start();
$footer_text =  'This footer is generated from php file';

if(!isset($_SESSION['username'])) {
	$log_in = "<a href='login.php'>Log in</a>";
	$register = "<a href='register.php'>Register</a>";
	$footer_text = "You are not logged in. $log_in $register";
} else {
	$username = $_SESSION['username'];
	$log_out = "<a href='log_out.php'>Log out</a>";
	$edit_data = "<a href='edit_data.php'>Edit account</a>";
	$footer_text = "Welcome $username!! $log_out $edit_data";
}
printf("<div class='my_footer'> $footer_text</div>");

 ?>
<?php
session_start();
if (!isset($_SESSION['username'])) {
    header('Location: login.php');
}
echo 'Welcome, ' . $_SESSION['username'];
?>

<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Submit your recipe</title>
		<meta name="keywords" content="cooking, recipes, form, poll">
		<meta name="description" content="This website will help you to share your best recipes with others.">
		<link rel="stylesheet" type="text/css" href="assets/base.css">
	</head>

	<body>
		<p>This paragraph should be highlighted as its first child</p>
		<div><p>This paragraph should be highlighted as its only child</p></div>
		<h1 id="backgroundRecipeSubmit">Submit your own recipe!</h1>
		<br><br>
		<p id="attentionText">Please fill out this form to share your recipe with other users.</p>

		<form class="customBorder" method="post" action="index.php">
			<input type="hidden" name="recipient" value="coo@king.com">
			<input type="hidden" name="subject" value="Recipe form">
			<input type="hidden" name="redirect" value="index.html">

			<p>
				<label>Your e-mail:<input name="e-mail" type="text" maxlength="30" disabled="disabled"></label>
			</p>

			<p>
				<label>Recipe title:<input required name="title" type="text" maxlength="30"></label>
			</p>

			<p>
				<label>Recipe description:<input required name="description" type="text" maxlength="200"></label>
			</p>

			<p>
				<label>
					<textarea name="description" rows="4" cols="36">Enter recipe here</textarea>
				</label>
			</p>

			<p>
				<label>
					Level of difficulty:
					<select name="difficulty">
						<option selected>Easy</option>
						<option>Medium</option>
						<option>Hard</option>
					</select>
				</label>
			</p>

			<p>
				<strong>Preparation time:</strong><br>

				<label>
					Less than 30 minutes
					<input name="preparationtime" type="radio" value="fast">
				</label>

				<label>Less than 1 hour
					<input name="preparationtime" type="radio" value="normal">
				</label>

				<label>More than 1 hour
					<input name="preparationtime" type="radio" value="long">
				</label>
			</p>

			<p>
				<strong>Type of meal:</strong><br>

				<label>
					Breakfast
					<input name="mealtype" type="checkbox" value="breakfast">
				</label>

				<label>Lunch
					<input name="mealtype" type="checkbox" value="lunch">
				</label>

				<label>Dinner
					<input name="mealtype" type="checkbox" value="dinner">
				</label>
			</p>

			<p>
				<input type="submit" value="Submit">
				<input type="reset" value="Clear">
			</p>
			<p></p>
			<p>This paragraph should be highlighted as its last child</p>
		</form>
		<?php include("footer_log_info.php"); ?>
	</body>
</html>
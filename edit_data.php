<?php 
include('server.php');
?>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Edit data</title>
		<link rel="stylesheet" type="text/css" href="assets/base.css">
	</head>
<body style="margin-top:20px;">
<form method="post">
	<h2 class="info"> Edit your account </h2>
	<?php include('errors.php'); ?>
	<table cellpadding="2" cellspacing="2">
		<tr>
			<td>Login</td>
			<td><input type="text" name="login"></td>
		</tr>
		<tr>
			<td>Old Password</td>
			<td><input type="password" name="password"></td>
		</tr>
		<tr>
			<td>New password</td>
			<td><input type="password" name="password_new"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" name="editUserData" value="Edit data"></td>
		</tr>
	</table>
</form>
</body>
</html>
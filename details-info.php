<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cooperation</title>
	<meta name="keywords" content="cooking, photos, food, contact, cooperation">
	<meta name="description" content="Personal contact.">
</head>
<body>
	<h2>Fill optional detailed info about you</h2>
	<form method="post" action="index.php">
		
		<label>Favorite <strong>color</strong></label>
		<input type="color" value="#ff0000">
		<br><br>
		
		<label>Favorite <b>month</b> of the year</label>
		<input type="month">
		<br><br>
		
		<label>Rate website in <b>1-10</b> scale</label>
		<input type="range" min="1" max="10" value="9">
		<br><br>
		
		<label>Favorite website</label>
		<input type="url" name="userUrl" placeholder="http://www.example.com">
		<br><br>
		
		<label>Search recipe:</label>
		<input type="search" name="search" placeholder="Super brownies">
		<br><br><br>

		<input type="submit">
	</form>
	<?php include("footer_log_info.php"); ?>
</body>
</html>
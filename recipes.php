<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Recipes</title>
	<meta name="keywords" content="cooking, recipes, ideas, food">
	<meta name="description" content="This website will help you to cook a delicious meal providing selected recipes.">
	<link rel="stylesheet" type="text/css" href="assets/base.css">
	<script type="text/javascript" src="./js/recipes.js"></script>
	<script>
		var productsQuantitiesOrigin = [5, 1, 1, 0.5, 0.5];
		var vinegar = new Product('vinegar spoons', 0.10, 5);
		var milk = new Product('cup of milk', 0.30, 1);
		var eggs = new Product('egg', 0.6, 1);
		var soda = new Product('baking soda spoons', 0.9, 0.5);
		var salt = new Product('salt spoons', 0.05, 0.5);

		var products = [vinegar, milk, eggs, soda, salt];

        function start() {
            var eggsButton = document.getElementById("adjustByEggsButton");
            var milkButton = document.getElementById("adjustByMilkButton");
            var countTotalPrice = document.getElementById("showTotalPrice");
            eggsButton.addEventListener("click", adjustIngredientsByEgg, false);
            milkButton.addEventListener("click", adjustIngredientsByMilk, false);
            countTotalPrice.addEventListener("click", showTotalPriceAlert, false);
        }

        function showTotalPriceAlert() {
        	var minPrice = minProductsPrice(products);
        	var message;
        	switch(true) {
        		case minPrice < 2:
        			message = "Very low price. ";
        			break;
        		case minPrice < 10:
        			message = "Low price. ";
        			break;
        		case minPrice < 40:
        			message = "Affordable price. ";
        			break;
        		case minPrice > 500:
        			message = "Expensive as gold. ";
        			break;
        		default:
        			message = "";
        	}
        	window.alert(message + "Price of products: " + minPrice + "$");
        }

        function adjustIngredientsByEgg() {
            var numberOfEggs = window.prompt("Provide number of eggs:");
            if (isValidNumberForRecipe(numberOfEggs)) {
                updateIngredientsTable(parseInt(numberOfEggs));
            } else {
                window.alert("Provide number between 1 and 999");
            }
        }

        function adjustIngredientsByMilk() {
            var amountOfMilk = window.prompt("Provide amount of milk:");
            if (isValidNumberForRecipe(amountOfMilk)) {
                updateIngredientsTable(parseFloat(amountOfMilk));
            } else {
                window.alert("Provide number between 1.0 and 999.0");
            }
        }

        function isValidNumberForRecipe(n) {
            return !isNaN(n) && isInCorrectRange(n);
        }

        function isInCorrectRange(n) {
			return n > 0 && n < 1000;
		}

		function updateQuantities(multiplier) {
			var i = 0;
			while(i < products.length) {
				products[i].quantity = productsQuantitiesOrigin[i] * multiplier;
				i++;
			}
		}

        function updateIngredientsTable(multiplier) {
        	updateQuantities(multiplier);
            var ingredientsTable = document.getElementById("tableDiv");
            ingredientsTable.innerHTML = "<table id='ingredientsTable'>" +
                "<caption><strong>Table of ingredients</strong></caption>" +
                "<thead><tr><th>Ingredient</th><th>Quantity</th></tr></thead>" +
                "<tbody>" +
                "<tr><td>tablespoons white vinegar</td><td>"+ vinegar.quantity +"</td></tr>" +
                "<tr><td>egg</td><td>" + eggs.quantity + "</td></tr>" +
                "<tr><td>cup milk</td><td>" + milk.quantity + "</td></tr>" +
                "<tr><td>tea spoon baking soda</td><td>" + soda.quantity + "</td></tr>" +
                "<tr><td>tea spoon salt</td><td>" + salt.quantity + "</td></tr>" +
                "<td colspan='2'>That's all!</td>" +
                "</tbody></table>";
        }

        window.addEventListener("load", start, false);
	</script>
</head>

<body>
<div class="header">
	<h1>Recipes</h1>
	<h3>your cooking ideas</h3>
</div>

<div id="parent">
	<div class="recipeRow">
		<div class="recipeSection">
			<h2 class="recipeTitle">Fluffy Pancakes</h2>
			<aside>
				"Keep calm and eat pancakes!"
			</aside>
			<img class="single" src="images/pancakes.jpeg" width="320" height="195" alt="Pancakes photo">
		</div>
		<div id="recipeDescription">
			<h4 id="pancakes-description">Description</h4>
			<p>
				<strong>Taste</strong>
				<meter min="1" max="10" value="9.5"></meter>
				<strong>Difficulty</strong>
				<meter min="1" max="10" value="6"></meter>
			</p>
			<span>Tall and fluffy.<span>These pancakes are just right. Topped with strawberries and whipped cream, they are
					impossible
					to
					resist.
				</span></span>
			<h4 id="pancakes-prep-time">Preparation time</h4>
			<details>
				<summary>45 minutes</summary>
				<p>
					Preparation time does include baking time!
				</p>
			</details>
		</div>
	</div>

	<div class="clear"></div>

	<div class="recipeRow">
		<form action="#">
			<input id="adjustByEggsButton" type="button" value="Adjust by # of eggs">
			<input id="adjustByMilkButton" type="button" value="Adjust by milk">
			<input id="showTotalPrice" type="button" value="Products price">
		</form>

		<div id="tableDiv" class="recipeSection">
			<table id="ingredientsTable">
				<caption><strong>Table of ingredients</strong></caption>
				<thead>
				<tr>
					<th>Ingredient</th>
					<th>Quantity</th>
				</tr>
				</thead>

				<tbody>
				<tr>
					<td>tablespoons white
						<mark>vinegar</mark>
					</td>
					<td>2</td>
				</tr>
				<tr>
					<td>
						<mark>egg</mark>
					</td>
					<td>1</td>
				</tr>
				<tr>
					<td>cup
						<mark>milk</mark>
					</td>
					<td>&frac34;</td>
				</tr>
				<tr>
					<td>teaspoon baking
						<mark>soda</mark>
					</td>
					<td>&frac12;</td>
				</tr>
				<tr>
					<td>teaspoon
						<mark>salt</mark>
					</td>
					<td>&frac12;</td>
				</tr>
				<tr>
					<td colspan="2">That's all!</td>
				</tr>
				</tbody>
			</table>
		</div>

		<div class="recipeSection">
			<h4 id="pancakes-guide">Step-by-step guide</h4>
			<ul>
				<li id="pancakes-guide-step1">Step 1 - Combine milk with vinegar in a medium bowl and set aside for 5
					minutes to
					"sour".
				</li>
				<li id="pancakes-guide-step2">Step 2 - Combine flour, sugar, baking powder, baking soda, and salt in a
					large
					mixing
					bowl. Whisk egg and butter
					into "soured" milk. Pour the flour mixture into the wet ingredients and whisk until lumps are gone.
				</li>
				<li id="pancakes-guide-step3">
					Step 3 - Heat a large skillet over medium heat, and coat with cooking spray. Pour 1/4 cupfuls of
					batter onto
					the
					skillet, and cook until bubbles appear on the surface. Flip with a spatula, and cook until browned
					on the
					other
					side.
				</li>
			</ul>
		</div>
	</div>

	<!-- <div class="footer"> -->
		<!-- <a href="images/pancakes.jpeg" download>Download photo of meal</a><br> -->
		<!-- <a href="https://i.pinimg.com/736x/83/e4/0c/83e40ca57f58acd1a09d2ba7956e48c2--best-veggie-burger-fast-food-chains.jpg" -->
		   <!-- download>Download second photo of meal</a> -->
		<!-- <footer>&#9400; All rights reserved.</footer> -->
	<!-- </div> -->
</div>
<?php include("footer_log_info.php"); ?>
</body>
</html>
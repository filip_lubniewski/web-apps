<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cooperation</title>
	<meta name="keywords" content="cooking, photos, food, contact, cooperation">
	<meta name="description" content="Personal contact.">
	<link rel="stylesheet" type="text/css" href="assets/base.css">
	<script type="text/javascript" src="./js/contact-form.js"></script>
</head>
<body>
	<?php 
		define("REGEX_FIRST_CAPITAL", '/^[A-Z].*/');
		define("REGEX_PHONE_NUMER", '/\+\d{2}\s*\d{3}\s*\d{3}\s*\d{3}/');

		$firstName = isset($_POST["first-name"]) ? $_POST["first-name"] : "" ;
		$secondName = isset($_POST["second-name"]) ? $_POST["second-name"] : "";	
		$email = isset($_POST["e-mail"]) ? $_POST["e-mail"] : "";
		$birthMonth = $_POST["birth-month"];
		$mobileNumber = $_POST["user-mobile"];
		$weight = $_POST["weight"];
		$allergies = isset($_POST['fromPerson']) ? $_POST["allergies"] : array();

		if(empty($firstName)){
			print_error_div('First name not specified.');
		}
		else if(!preg_match(REGEX_FIRST_CAPITAL, $firstName)) {
			print_error_div('First name not starting with capital letter.');
		} else {
			print_correct_div('Correct first name.');
		}

		if(empty($secondName)) {
			print_error_div('Second name not specified.');
		} else if(!preg_match(REGEX_FIRST_CAPITAL, $secondName)) {
			print_error_div('Second name not starting with capital letter.');
		} else {
			print_correct_div('Correct second name.');
		}

		if(empty($email)) {
			print_error_div('Email not specified.');
		}
		else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			print_error_div('Invalid email.');
		} else {
			print_correct_div('Correct email.');
		}

		if(empty($mobileNumber)) {
			print_error_div('Mobile number not specifed.');
		} else if(!preg_match(REGEX_PHONE_NUMER, $mobileNumber)) {
			print_error_div('Invalid mobile number.');
		} else {
			print_correct_div('Correct mobile number.');
		}

		if(empty($weight)) {
			print_error_div('Weight not specified.');
		} else {
			$weight = preg_replace('/\s*kg\s*/', '', $weight);
			if(!preg_match('/(-)?\d+(\.\d{1})?/', $weight)) {
				print_error_div('Weight not specified.');
			} else if((double)$weight <= 0) { //casting
				print_error_div('Weight must be greater than zero!');
			} else if($weight > 1000) {
				print("<script type='text/javascript'> 
						alert('Your weight is out of control!'); 
						window.location = '/pwr/contact-form.html';
					</script> ");
				die();
			} else {
				print_correct_div('Weight specified.');
				print_weight_info_div($weight);
			}
		}

		if(count($allergies) == 0) {
			print_correct_div('You have no allergies.');
		} else {
			print("<div class='error' style='float:left; margin-right: 5px;'> You are allerged to: </div>");
			foreach ($allergies as $allergy) {
				print_allergy_div($allergy);
			}
			reset($allergies);
			$firstAllergy = current($allergies);
			print("<div id='allergy-cure-promo' style='clear:both;'>We can help you with your first allergy - $firstAllergy FOR FREE!</div>");
			print("<div class='error' style='clear:both;'>Carefully select recipes!</div>");
		}
	
		$userIp = get_client_ip();
		print("<div id='ip-message'>Your ip address is: $userIp</div>");

		function get_client_ip() {
		    $ipaddress = '';
		    if (getenv('HTTP_CLIENT_IP'))
		        $ipaddress = getenv('HTTP_CLIENT_IP');
		    else if(getenv('HTTP_X_FORWARDED_FOR'))
		        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		    else if(getenv('HTTP_X_FORWARDED'))
		        $ipaddress = getenv('HTTP_X_FORWARDED');
		    else if(getenv('HTTP_FORWARDED_FOR'))
		        $ipaddress = getenv('HTTP_FORWARDED_FOR');
		    else if(getenv('HTTP_FORWARDED'))
		       $ipaddress = getenv('HTTP_FORWARDED');
		    else if(getenv('REMOTE_ADDR'))
		        $ipaddress = getenv('REMOTE_ADDR');
		    else
		        $ipaddress = 'UNKNOWN';
		    return $ipaddress;
		}

		function print_allergy_div($allergy) {
			print("<div class='allergy'>$allergy</div>");
		}

		function print_error_div($message) {
			print("<div class='error'>$message</div>");
		}

		function print_correct_div($message) {
			print("<div class='correct'>$message</div>");
		}

		function print_weight_info_div($user_weight) {
			$result = get_weight_category_and_perfect_diff($user_weight);
			$user_category = $result["category"];
			$diff_perfect = $result["diff_perfect"];
			$categories = $result["categories"];
			$categories_sum_up = "";
			foreach($categories as $category) {
				$categories_sum_up .= $category . " ";
			}
			$message = "You are $user_category. Info based on your weight: $user_weight kg. 
						<br> You are $diff_perfect kg from perfect weight. <br> Categories were: $categories_sum_up";
			print("<div id='weight-message'>$message</div>");
		}

		function get_weight_category_and_perfect_diff($user_weight) {
			$weights_borders =  array (
				"PERFECT" => 100,
				"OBESE" => 200,
				"SKINNY" => 50,	
			);
			$result_array = array (
				"category" => "",
				"diff_perfect" => 0,
				"categories" => array(),
				);
			if($user_weight <= $weights_borders["SKINNY"]) {
				$result_array["category"] = "skinny";
			} else if($user_weight <= $weights_borders["PERFECT"]) {
				$result_array["category"] = "perfect";
			} else if($user_weight <= $weights_borders["OBESE"]) {
				$result_array["category"] = "obese";
			}
			$result_array["diff_perfect"] = abs($weights_borders["PERFECT"] - $user_weight); // dynamic casting

			for($i = 0; $i < count($weights_borders); $i++) {
				$result_array["categories"][] = key($weights_borders);
				next($weights_borders);
			}
			return $result_array;
		}
	?>
	<form action="contact-form.html">
    	<input type="submit" value="Go back!" />
	</form>
	<?php include("footer_log_info.php"); ?>
</body>
</html>
<?php 
$host = "localhost";
$user = "root";
$password = "";
$dbname = "web_test_db";

$db = new mysqli($host, $user, $password, $dbname)  or die("Unable to connect to database"); 

if (mysqli_connect_errno())
{
    printf("Wystąpił błąd: ", mysqli_connect_error());
    exit;
}

$table_name = 'users';
$sql_columns = "SHOW COLUMNS FROM $table_name";
$result_columns = $db->query($sql_columns);

$sql = $db->query("SELECT * FROM $table_name");
$record_number = $sql->num_rows; // odpowiednik mysqli_num_rows
echo '<p>Liczba rekordów tabeli: '.$record_number.'</p>';

if ($record_number > 0) {
	printf("<table cellspacing='10'>");
	while($row_col = $result_columns->fetch_assoc()){
    	printf("<th>".$row_col['Field']."</th>");
	}
	while($row = $sql -> fetch_assoc()) {
		// fetch_assoc - zwraca tablicę asocjacyjną pobranych wierszy, lub FALSE jeżeli nie ma więcej wyników
		printf("<tr>");
		$result_columns->data_seek(0);
		while($row_col = $result_columns->fetch_assoc()){
    		printf("<td>".$row[$row_col['Field']]."</td>");
		}	
		printf("</tr>");
	}
	printf("</table>");
}

$sql->free(); // zwalniamy pamięć
$db->close(); // zamykamy połączenie z bazą
 ?>
